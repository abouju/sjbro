package sjbro;

import java.io.InputStream;

import java.util.StringTokenizer;

/*Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Alain Bouju

Ce logiciel est un programme informatique servant à "SJBRO Simple Java BROwser". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Alain Bouju

This software is a computer program whose purpose is to SJBRO Simple Java BROwser.

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/
/**
 * Render
 *
 * @version 	0.1, 06 Janvier 2004
 * @author A.Bouju
 */


 
 public class Render  
{
	/** la methode <CODE>renderString(InputStream pIn)/CODE> affiche un fichier HTML
	TODO Cache 
	@param pIn flux input
	@see InputStream
	*/
	 	 
	 
	 // ----------------- Local Parser
 boolean startEnd = true; // true = start end = false
 public void renderString(InputStream pIn)
 {
  Tag currentTag = null;
  System.out.println("Render ");
  
  try{
  
  int nbCar = -1;
  do{
  	nbCar = pIn.read();
  	char car = (char) nbCar;
	startEnd = true;
	// Start Tag reading 
    if (car=='<')
    	{
			String sTag = readTag(pIn);
			if (!startEnd)
			System.out.println("Tag Fin");
			else
			 {
			 
			  currentTag =  initTag (sTag);
			 }
			
    	}
    else
	System.out.print(" "+car);
  }while (nbCar!=-1);
  }
  catch (java.io.IOException ex) {
			 ex.printStackTrace();
		 }
  
 }
 
 public Tag initTag(String pString)
 {
 Tag tag=null;
 String currentStart;
 String currentStartLower;
 System.out.println("Recherche Tag "+pString);
 StringTokenizer stringTokenizer = new StringTokenizer(pString);
 if (stringTokenizer.hasMoreTokens())
 {
 // First Token
 currentStart = stringTokenizer.nextToken();
 currentStartLower = currentStart.toLowerCase();
 System.out.println("+--+ " +currentStart+" +--+ "+ currentStartLower);
 if (currentStartLower.startsWith("html"))
 	{
	 System.out.println("HTML");
	}
 else if (currentStartLower.startsWith("head"))
 	{
	 System.out.println("HEAD");
	}
 else if (currentStartLower.startsWith("title"))
 	{
	 System.out.println("TITLE");
	}
 else if (currentStartLower.startsWith("body"))
 	{
	 System.out.println("BODY");
	}
 }
 while (stringTokenizer.hasMoreTokens()) {
 	    String current = stringTokenizer.nextToken();
	    String currentLower = current.toLowerCase();
            System.out.println("++++ " +current+" ++++ "+ currentLower);
        }
 return tag;
 }
 
 public String readTag(InputStream pIn)
 {
 	boolean firstCar=true;
 	String sTag= new String();
	int nbCar = -1;
	char car;
	try{
	do{
	  nbCar = pIn.read();
	  car = (char) nbCar;
	  if ((firstCar)&&(car=='/')) startEnd=false;

	  sTag = sTag + car;
	}while (car!='>');
	
	}
	catch (java.io.IOException ex) {
			   ex.printStackTrace();
		   }
	System.out.println("#<"+sTag+"#");
 	return sTag;
 }
 
 
 
}