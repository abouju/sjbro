package sjbro;

import org.fit.cssbox.layout.Dimension;
import org.fit.cssbox.css.DOMAnalyzer;
import org.fit.cssbox.io.DOMSource;
import org.fit.cssbox.io.DefaultDOMSource;
import org.fit.cssbox.io.DocumentSource;
import org.fit.cssbox.layout.BrowserCanvas;
import org.fit.cssbox.layout.BrowserConfig;
import org.fit.cssbox.css.CSSNorm;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import cz.vutbr.web.css.MediaSpec;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


/*Copyright 

Laboratoire L3i
Université de La Rochelle
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

contributeurs :
Alain Bouju

Ce logiciel est un programme informatique servant à "SJBRO Simple Java BROwser". 

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Copyright 
L3i Laboratory
La Rochelle University
Avenue Michel Crépeau
17042 La Rochelle Cedex 1 - France

authors :
Alain Bouju

This software is a computer program whose purpose is to SJBRO Simple Java BROwser.

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


/**
 * RenderCSSBox
 *
 * @version 	0.1, 09 Janvier 2020
 * @author A.Bouju
 */
public class RenderCSSBox {
	 BrowserCanvas canvas;
	
	
public RenderCSSBox(MyJTabbedPane myJTabbedPane,URL url, InputStream pIn,String  type) {
		 int iTab=myJTabbedPane.getSelectedIndex();
		 System.out.println("iTab "+iTab);
		 // Document source init
		 DocumentSource docSource=null;
		try {
			docSource = new MyDocumentSource(url,pIn,type);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Parse the input document
	     DOMSource parser = new DefaultDOMSource(docSource);
	     Document doc=null;
		try {
			doc = parser.parse();
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//create the media specification
		Dimension  windowSize = new Dimension(1200, 600);
        
	   //Create the CSS analyzer
	     DOMAnalyzer da = new DOMAnalyzer(doc, docSource.getURL());
	     da.attributesToStyles(); //convert the HTML presentation attributes to inline styles
	     da.addStyleSheet(null, CSSNorm.stdStyleSheet(), DOMAnalyzer.Origin.AGENT); //use the standard style sheet
	     da.addStyleSheet(null, CSSNorm.userStyleSheet(), DOMAnalyzer.Origin.AGENT); //use the additional style sheet
	     da.addStyleSheet(null, CSSNorm.formsStyleSheet(), DOMAnalyzer.Origin.AGENT); //render form fields using css
	     da.getStyleSheets(); //load the author style sheets
	     
	    
	     //Create the browser canvas
	     
	     canvas = new BrowserCanvas(da.getRoot(),  da /*,new Dimension(1000, 600)*/, url);
	     
	     canvas.getEngine().createLayout(windowSize);

		 myJTabbedPane.setComponentAt(iTab, canvas);
	     canvas.setVisible(true);
	     canvas.repaint();
	 }

public void renderCSSBox(URL url, InputStream pIn,String  type)
{
 System.out.println("start Render CSSBox");
 
 try{
	 /*
	 // Document source init
	 DocumentSource docSource = new MyDocumentSource(url,pIn,type);
	//Parse the input document
     DOMSource parser = new DefaultDOMSource(docSource);
     Document doc = parser.parse();
   //Create the CSS analyzer
     DOMAnalyzer da = new DOMAnalyzer(doc, docSource.getURL());
     da.attributesToStyles(); //convert the HTML presentation attributes to inline styles
     da.addStyleSheet(null, CSSNorm.stdStyleSheet(), DOMAnalyzer.Origin.AGENT); //use the standard style sheet
     da.addStyleSheet(null, CSSNorm.userStyleSheet(), DOMAnalyzer.Origin.AGENT); //use the additional style sheet
     da.addStyleSheet(null, CSSNorm.formsStyleSheet(), DOMAnalyzer.Origin.AGENT); //render form fields using css
     da.getStyleSheets(); //load the author style sheets
     //Create the browser canvas
     
     canvas = new BrowserCanvas(da.getRoot(), da, new Dimension(300, 300), url);
     canvas.setSize(1275, 750);
     canvas.setVisible(true);
     
     docSource.close();*/
 }
 catch (Exception ex) {
			 ex.printStackTrace();
		 }
 System.out.println("end Render CSSBox");

}

}
