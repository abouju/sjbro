#!/bin/sh 
# ==================================================
# Java Simple BROwser startup script
#    author  A. Bouju
#    date    11/02/2020
# ==================================================
export CLASSPATH=./bin/:./lib/antlr-runtime-4.5.3.jar:./lib/batik-all-1.12.jar:./lib/cssbox-5.0-SNAPSHOT.jar:./lib/jstyleparser-3.6-SNAPSHOT.jar:./lib/logback-classic-1.2.3.jar:./lib/logback-core-1.2.3.jar:./lib/logback-core-1.2.3.jar:./lib/slf4j-api-1.7.30.jar:./lib/unbescape-1.1.7.BUILD-SNAPSHOT.jar:./lib/xercesImpl.jar:./lib/xml-apis-ext-1.3.04.jar:./lib/xmlgraphics-commons-2.4.jar:./lib/nekohtml.jar
echo $CLASSPATH
java sjbro.Main